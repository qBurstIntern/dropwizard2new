package com.qburst.dropwizard.application;

import com.datastax.driver.mapping.MappingManager;
import com.qburst.dropwizard.configuration.MainConfiguration;
import com.qburst.dropwizard.db.CassandraConnection;
import com.qburst.dropwizard.db.MyDAO;
import com.qburst.dropwizard.resource.APIResource;
import com.qburst.dropwizard.resource.LoginResource;
import com.qburst.dropwizard.resource.PersonHomeResource;
import com.qburst.dropwizard.resource.SignupResource;
import com.qburst.dropwizard.resource.exceptionsservice.RunTimeExceptionMapper;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;

import java.util.Map;

/**
 * Created by sreeraj on 23/4/15.
 */
public class MainApplication extends Application<MainConfiguration> {

    public static void main(String[] args) throws Exception {
        new MainApplication().run(args);
    }

    public void initialize(Bootstrap<MainConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<MainConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(MainConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", null));
        bootstrap.addBundle(new AssetsBundle("/assets/favicon.ico", "/assets/favicon.ico", null, "favicon"));
    }

    @Override
    public void run(MainConfiguration configuration, Environment environment) {

        MappingManager manager = new MappingManager (new CassandraConnection(configuration.getCassandradbFactory().getContactPoint(),configuration.getCassandradbFactory().getKeySpace()).getSession());
        MyDAO dao = manager.createAccessor(MyDAO.class);

        environment.jersey().register(new SignupResource(dao));
        environment.jersey().register(new LoginResource(dao));
        environment.jersey().register(new PersonHomeResource(dao));
        environment.jersey().register(new APIResource(dao));

        environment.servlets().setSessionHandler(new SessionHandler());
        environment.jersey().register((new RunTimeExceptionMapper()));


    }
}
