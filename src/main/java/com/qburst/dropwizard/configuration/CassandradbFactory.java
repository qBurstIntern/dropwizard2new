package com.qburst.dropwizard.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by sreeraj on 4/5/15.
 */
public class CassandradbFactory {


    @NotEmpty
    private String contactPoint;

    @NotEmpty
    private String keySpace;

    @JsonProperty
    public String getContactPoint() {
        return contactPoint;
    }

    @JsonProperty
    public void setContactPoint(String contactPoint) {
        this.contactPoint = contactPoint;
    }

    @JsonProperty
    public String getKeySpace() {
        return keySpace;
    }

    @JsonProperty
    public void setKeySpace(String keySpace) {
        this.keySpace = keySpace;
    }


}
