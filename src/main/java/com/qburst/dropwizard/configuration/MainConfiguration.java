package com.qburst.dropwizard.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Map;

/**
 * Created by sreeraj on 23/4/15.
 */
public class MainConfiguration extends Configuration {

    @Valid
    @NotNull
    private CassandradbFactory cassandradb = new CassandradbFactory();

    @JsonProperty("cassandradb")
    public CassandradbFactory getCassandradbFactory() {
        return cassandradb;
    }

    @JsonProperty("cassandradb")
    public void setCassandradbFactory(CassandradbFactory factory) {

        this.cassandradb = factory;
    }

    @NotNull
    private Map<String, Map<String, String>> viewRendererConfiguration = Collections.emptyMap();


    @JsonProperty("viewRendererConfiguration")
    public Map<String, Map<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration;
    }

    @JsonProperty("viewRendererConfiguration")
    public void setViewRendererConfiguration(Map<String, Map<String, String>> viewRendererConfiguration) {
        ImmutableMap.Builder<String, Map<String, String>> builder = ImmutableMap.builder();
        for (Map.Entry<String, Map<String, String>> entry : viewRendererConfiguration.entrySet()) {
            builder.put(entry.getKey(), ImmutableMap.copyOf(entry.getValue()));
        }
        this.viewRendererConfiguration = builder.build();
    }
}
