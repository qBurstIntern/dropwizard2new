package com.qburst.dropwizard.core;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sreeraj on 23/4/15.
 */
@Table(keyspace = "qburst", name = "login")
public class Person {

    @PartitionKey
    @JsonProperty
    private String username;

    @JsonProperty
    private String name;

    @JsonProperty
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person() {

    }
    public Person(String username, String user) {
        this.username = username;
        this.name = user;
    }

    public Person(Person p) {
        this.name = p.getName();
        this.username = p.getUsername();
        this.password = p.getPassword();
    }
}
