package com.qburst.dropwizard.db;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/**
 * Created by sreeraj on 11/5/15.
 */
public class CassandraConnection {


    private Cluster cluster;

    public Cluster getCluster() {
        return cluster;
    }
    private Session session;
    public Session getSession() {
        return session;
    }


    public  CassandraConnection(String host,String keyspace)
    {
        cluster = Cluster.builder().addContactPoint(host).withCredentials("joseph", "asdf").build();
        session = cluster.connect(keyspace);

    }

}
