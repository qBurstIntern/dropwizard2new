package com.qburst.dropwizard.db;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.qburst.dropwizard.core.Person;

import java.util.List;


/**
 * Created by sreeraj on 23/4/15.
 */

@Accessor
public interface MyDAO {


    @Query("insert into login (name, username , password) values (:user, :username , :password)")
    ResultSet insert(@Param("user") String user, @Param("username") String username, @Param("password") String password);

   @Query("select * from login where username = :username ")
    Person getPerson(@Param("username") String username);


    @Query("select * from login  where username = :username")
    Person getHashForUser(@Param("username") String username);

    @Query("select count(*) from login  where username = :username")
    ResultSet getAvailability(@Param("username") String username);



}
