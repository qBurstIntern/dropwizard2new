package com.qburst.dropwizard.resource;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.qburst.dropwizard.core.Person;
import com.qburst.dropwizard.db.MyDAO;
import com.qburst.dropwizard.resource.Helper.PasswordHash;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by sreeraj on 29/4/15.
 */
@Path("/api")
public class APIResource {
    private final MyDAO dao;

    public APIResource(MyDAO dao) {
        this.dao = dao;
    }

    @Path("/signup")
    @POST
    public String getSignup(@FormParam("name") String name,
                            @FormParam("username") String username,
                            @FormParam("password") String password,
                            @FormParam("repassword") String repassword) {

        String messageServer = "";
        name = name.trim();
        username = username.trim();
        if (name.equals("")) {
            messageServer = "Name field is empty";
        } else if (name.length() < 3) {
            messageServer = "Name should contain atleast 3 characters";
        } else if (name.length() > 30) {
            messageServer = "Name should contain atmost 30 characters";
        } else if (username.equals("")) {
            messageServer = "Username is empty";
        } else if (username.length() < 6) {
            messageServer = "Username should contain atleast 6 characters";
        } else if (username.length() > 20) {
            messageServer = "Username should contain atmost 20 characters";
        } else if (password.equals("")) {
            messageServer = "Password field is empty";
        } else if (password.equals("")) {
            messageServer = "Password field is empty";
        } else if (!password.equals(repassword)) {
            messageServer = "Passwords do not match";
        } else {
            String hash;
            Integer isExist = 0;
            try {
                ResultSet isExistResultSet = dao.getAvailability(username);
                for (Row row : isExistResultSet) {
                    Long l = row.getLong(0);
                    isExist =  l.intValue();
                }
                if (isExist == null || isExist == 0) {
                    hash = PasswordHash.createHash(password);

                    if (dao.insert(name, username, hash).wasApplied()) {
                        messageServer = "success";
                    }
                } else {
                    messageServer = username + " is not available";
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
        return messageServer;
    }


    @Path("/login")
    @POST
    public String getLogin(@FormParam("username") String username, @FormParam("password") String password, @Session HttpSession session) {
        String messageServer = "";
        Person person = dao.getHashForUser(username);
        String hash =null;
        if(person == null){

        }else {
            hash = person.getPassword();
        }
        if (hash == null || hash.isEmpty()) {
            messageServer = "Invalid Credentials";
        } else {
            try {
                if (PasswordHash.validatePassword(password, hash)) {
                        session.setAttribute("username", username);
                        messageServer = "success";
                } else {
                    messageServer = "Invalid Credentials";
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
        return messageServer;
    }


}
