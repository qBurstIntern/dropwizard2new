package com.qburst.dropwizard.resource.Helper;


import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by sreeraj on 23/4/15.
 */
public class Utilities {


    public static Response redirectToURI(String newURI) {

        URI uri = null;
        try {
            uri = new URI(newURI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Response.seeOther(uri).build();

    }
}
