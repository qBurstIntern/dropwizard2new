package com.qburst.dropwizard.resource;

import com.qburst.dropwizard.db.MyDAO;
import com.qburst.dropwizard.resource.Helper.Utilities;
import com.qburst.dropwizard.view.LoginView;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by sreeraj on 23/4/15.
 */

@Path("/{parameter: |login}")
@Produces(MediaType.TEXT_HTML)
public class LoginResource {

    private final MyDAO dao;

    public LoginResource(MyDAO dao) {
        this.dao = dao;
    }

    @GET
    public Object getLogin(@Session HttpSession session) {

        if (session.getAttribute("username") == null || session.getAttribute("username").equals("")) {
            return new LoginView();
        } else {
            return Utilities.redirectToURI("/home");
        }

    }
}
