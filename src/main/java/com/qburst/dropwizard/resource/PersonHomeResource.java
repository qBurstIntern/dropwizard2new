package com.qburst.dropwizard.resource;

import com.qburst.dropwizard.core.Person;
import com.qburst.dropwizard.db.MyDAO;
import com.qburst.dropwizard.resource.Helper.Utilities;
import com.qburst.dropwizard.view.PersonView;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sreeraj on 24/4/15.
 */
@Path("/home")
@Produces(MediaType.TEXT_HTML)
public class PersonHomeResource {
    private final MyDAO dao;

    public PersonHomeResource(MyDAO dao) {
        this.dao = dao;
    }

    @GET
    public Object getPerson(@Session HttpSession session) {

        String username = (String) session.getAttribute("username");
        if (username == null || username.isEmpty()) {
            session.setAttribute("username", "");
            return Utilities.redirectToURI("/login");
        } else {
            Person p= dao.getPerson(username);
            if (p == null || p.getUsername()=="") {
                session.setAttribute("username", "");
                return Utilities.redirectToURI("/login");
            } else {
                return new PersonView(p);
            }
        }
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Object logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.invalidate();
        return Utilities.redirectToURI("/login");

    }

}
