package com.qburst.dropwizard.resource;

import com.qburst.dropwizard.db.MyDAO;
import com.qburst.dropwizard.view.SignupVIew;
import io.dropwizard.views.View;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by sreeraj on 23/4/15.
 */

@Path("/signup")
@Produces(MediaType.TEXT_HTML)
public class SignupResource {

    private final MyDAO dao;

    public SignupResource(MyDAO dao) {
        this.dao = dao;
    }

    @GET
    public View getSignup() {

        return new SignupVIew();

    }

}
