package com.qburst.dropwizard.resource.exceptionsservice;

import com.qburst.dropwizard.view.ErrorView;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by sreeraj on 27/4/15.
 */
public class RunTimeExceptionMapper implements ExceptionMapper<RuntimeException> {

    public Response toResponse(RuntimeException e) {
        e.printStackTrace();
        return Response
                .serverError()
                .entity(new ErrorView())
                .build();
    }
}
