package com.qburst.dropwizard.view;

import io.dropwizard.views.View;

/**
 * Created by sreeraj on 27/4/15.
 */
public class ErrorView extends View {

    public ErrorView() {
        super("500.ftl");
    }
}
