package com.qburst.dropwizard.view;

import io.dropwizard.views.View;

/**
 * Created by sreeraj on 23/4/15.
 */
public class LoginView extends View {

    String messageFromServer;


    public void setMessageFromServer(String messageFromServer) {
        this.messageFromServer = messageFromServer;
    }

    public String getMessageFromServer() {
        return messageFromServer;
    }



    public LoginView() {
        super("login.ftl");
        messageFromServer = "";
    }
}
