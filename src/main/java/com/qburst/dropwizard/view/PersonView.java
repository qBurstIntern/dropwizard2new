package com.qburst.dropwizard.view;


import com.qburst.dropwizard.core.Person;
import io.dropwizard.views.View;

/**
 * Created by sreeraj on 22/4/15.
 */
public class PersonView extends View {

    private final Person person;

    public PersonView(Person person) {
        super("person.ftl");
        this.person = person;
    }

    public PersonView(String userName, String user) {
        super("person.ftl");
        this.person = new Person(userName, user);

    }


    public Person getPerson() {
        return person;
    }


}
