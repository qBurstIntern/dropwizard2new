package com.qburst.dropwizard.view;

import io.dropwizard.views.View;

/**
 * Created by sreeraj on 23/4/15.
 */
public class SignupVIew extends View {

    String messageFromServer;

    public void setMessageFromServer(String messageFromServer) {
        this.messageFromServer = messageFromServer;
    }

    public String getMessageFromServer() {
        return messageFromServer;
    }



    public SignupVIew() {
        super("signup.ftl");
        messageFromServer = "";

    }

}
