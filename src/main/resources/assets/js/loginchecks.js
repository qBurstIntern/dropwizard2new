  
  // When the browser is ready...
  $(function() {
       // Setup form validation on the #login-form element
    $("#login-form").validate({
    
        // Specify the validation rules
        rules: {
            username: {
                required: true          
            },
            password: {
                required: true
              
            }
        },
        
        // Specify the validation error messages
        messages: {
           
            username: "Please enter username",
            password: {
                required: "Please enter Password"
                
            }
        },
        submitHandler: function(form) {

            $.post( "/api/login", {
                username: $('#username').val(),
                password: $('#password').val(),
            }).done(function(data) {
                if (data === "success") {
                    window.location.href = '/home';
                } else {
                    $('#status').text(data);
                }
            });
        }
    });

  });
