  
  // When the browser is ready...
  $(function() {
       // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            name: "required",
            username: {
                required: true,
                minlength: 6
            },
            password: {
                required: true,
                minlength: 3
            },
           repassword:{
             required: true,
             equalTo: "#password"
           }
        },
        
        // Specify the validation error messages
        messages: {
            name: "Please enter your Name",
            username:{
             required: "Please enter username",
             minlength: "Username should contain 6 letters"
            },
            password: {
                required: "Please enter Password",
                minlength: "password should contains atleast 6 characters"
            },
            repassword: {
                required: "Please reenter Password",
                equalTo: "passwords don't match"
            }
           
        },
        submitHandler: function(form) {

            $.post( "/api/signup", {
                name: $('#name').val(),
                username: $('#username').val(),
                password: $('#password').val(),
                repassword: $('#repassword').val()
            }).done(function(data) {
                if (data === "success") {
                    window.location.href = '/login';
                } else {
                    $('#status').text(data);
                }
            });
        }
    });

  });
