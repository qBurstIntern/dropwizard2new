<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="icon" type="image/x-icon" href="/public/favicon.ico"/>
		<title>
		Login

		</title>
	</head>
	<body>
		<div class="container">
			<form action="" method="POST" id="login-form" class="form-horizontal">
				<table class="table">
					<tr>
						<td>
							<h2>Login Now.............</h2>
						</td>
					</tr>
					<tr>
						<td><h4><label id="status" class="text-danger">
							
						</label></h4></td>
					</tr>
					<tr>
						<td><label>UserName</label></td>
						<td><input type="text" name="username" id="username" value="" class="form-control" /></td>
					</tr>
					<tr>
						<td><label>
							Password
							</label>
						</td>
						<td><INPUT TYPE="password" name="password" value="" class="form-control" id="password" /></td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="submit" value="Login" class="btn btn-primary">
						</td>
					</tr>
					<tr>
						<td>
							<a href="/signup">
								Sign Up Now
							</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<script src="//code.jquery.com/jquery-1.11.2.min.js"> </script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
		<script type="text/javascript" src="/public/js/loginchecks.js"></script>
	</body>
</html>