<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <link rel="icon" type="image/x-icon" href="/public/favicon.ico"/>
        <title>Hello</title>
    </head>
    <body>
        <div class="jumbotron">
            <h1>Hello, ${person.name?html}!</h1>
            <form method="POST">
                <input class="btn btn-success" type="submit" value="Logout">
            </form>
        </div>
        <script src="//code.jquery.com/jquery-1.11.2.min.js"> </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </body>
</html> 
