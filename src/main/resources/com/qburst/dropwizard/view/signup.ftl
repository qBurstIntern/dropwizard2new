<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="icon" type="image/x-icon" href="/public/favicon.ico"/>
		<title>
		SignUP
		</title>
  	</head>
	<body>
		<div class = "container" >
			<form action = "" method = "POST" id ="register-form" class="form-horizontal">
				<table class="table">
					<tr>
						<td>
							<h1> 
							<label>SignUp Now............. </label></h1>
						</td>
					</tr>
					<tr>
						<td>
						<label class="text-danger" id="status">
							<h4></h4>
						</label>
						</td>
					</tr>
					<tr>
						<td>
						<label>Name</label></td>
						<td><input type = "text" id="name" name ="name" value = "" class="form-control" placeholder="Your Name"/> </td>
					</tr>
					<tr>
						<td><label>Username</label></td>
						<td> <input type = "text" id="username" name="username" value = "" class="form-control"placeholder="Username"/></td>
					</tr>
					<tr>
						<td><label>Password</label></td>
						<td><INPUT TYPE="password" id="password" name="password" value="" class="form-control" placeholder="Password"/></td>
					</tr>
					<tr>
						<td><label>Reenter Password</label></td>
						<td><INPUT TYPE="password" name="repassword" id="repassword" value="" class="form-control" placeholder="Reenter Password"/> </td>
					</tr>
					<tr><td></td>
						<td colspan="2"> <input type = "submit"	name="submit" value = "SignUP" id = "checkSignup" class="btn btn-primary" > </td>

					</tr>
					<tr><td><a href="/login" class="btn btn-default btn-lg active" role="button">Or Login </a></td></tr>
	
				</table>
			</form>
			
		</div>
		<script src="//code.jquery.com/jquery-1.11.2.min.js" > </script>
  		<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
		<script type="text/javascript" src="/public/js/signupchecks.js"></script>
	</body>
</html>